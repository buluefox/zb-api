package zb.ws.api;

import java.io.IOException;

import lombok.val;
import ws.api.ApiConfig;
import zb.entity.UserApiEntity;
import zb.enumtype.ExchangeEnum;
import zb.ws.adapter.AdapterZb;

public class TestWsApi {

	public static void main(String[] args) throws IOException {
		
		val userApi = new UserApiEntity(ExchangeEnum.zb, "测试", "","");
		val apiConfig = new ApiConfig("wss://api.zb.com:9999/websocket", "192.168.5.182", 23128);
		val api = new WsZb(userApi, new AdapterZb());
		String symbol= "eth_usdt";
//		api.depth(symbol);
//		api.saveChannel("ltcusdt");
		api.getAccount();
		
//		api.buy(symbol, 650, 0.001);
//		api.getOrder(symbol, 20180522105585216l, "测试");
//		api.cancel(symbol, 20180522105585216l, "取消");
	}
}
