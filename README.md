# zb-api
封装www.zb.com行情/交易api 只为更简单的调用 自从国内交易所被墙之后 接口变得不稳定 这个项目初衷是为了支持翻墙而写,联系方式:  http://qiongbi.chenwenxi.cc/

> 环境:maven,jdk8


## 接口使用说明

### 项目Project

~~~
wechat         模块目录

├─zb-api-core              多个project需要共用的类
├─zb-api-ws                zb websocket示例
├─zb-api-rest              zb rest示例
├─zb-api-strategy          zb 简单策略demo

 ... 持续更新中...
~~~

## zb-api-strategy 策略
- 更新ma值计算
![输入图片说明](https://gitee.com/uploads/images/2018/0605/172633_cbeb1c60_3577.png "ma.png")

## zb-api-rest 接口调用

![输入图片说明](https://gitee.com/uploads/images/2018/0604/192735_ff555508_3577.png "QQ截图20180604192427.png")


## zb-api-ws 行情接口

~~~
    saveChannel 保存订阅信息(如果断开连接后,重新握手会重新加载订阅)
    ... 持续更新中...
~~~

## zb-api-ws 交易接口

~~~
    getAccount 用户信息
    buy 限价订单买入
    sell 限价订单卖出
    ... 持续更新中...
~~~

## 进行翻墙连接 
```java
WebSocketFactory factory = new WebSocketFactory();
ProxySettings settings = factory.getProxySettings();
settings.setHost("fq12138.com").setPort(23128);
```
## 测试 zb.ws.api.TestWsApi.java
```java
String apiKey = p.getValue("apiKey");
String secretKey = p.getValue("secretKey");
UserApiEntity userApiEntity = new UserApiEntity(ExchangeEnum.zb, "账户备注", apiKey, secretKey);
WsZb api = new WsZb(userApiEntity, new ZbAdapter());
api.saveChannel("ltcqc_ticker");//订阅行情信息
api.getAccount();//用户信息
api.sell("ltcqc", 1300, 0.1); //卖出ltcqc
```

## 后续会陆续更新rest接口或者其他交易所..请敬请期待!!如果有问题可以联系我,或者去可怜可怜我并留言: