package test.rest.zb.demo;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import entity.commons.UserApi;
import entity.enumtype.ExchangeEnum;
import entity.enumtype.KTimeEnum;
import entity.exchange.DeptResult;
import entity.exchange.DeptResult.Dept;
import entity.exchange.KlineResult;
import jodd.props.Props;
import rest.zb.rest.api.RestApiGet;
import rest.zb.rest.api.RestApiPost;
import rest.zb.rest.entity.Order;
import rest.zb.rest.entity.TradeResult;
import rest.zb.rest.entity.post.Account;

public class Demo {
	private RestApiPost apiPost;
	private RestApiGet apiGet;
	
	@Test
	public void 行情api() {
		//获取k线图
		List<KlineResult> kline = apiGet.getKline(KTimeEnum.min1);
		KlineResult klineResult = kline.get(kline.size() - 1);
		System.out.println(klineResult.getDate()+" "+klineResult);
		
		//获取买卖方所有深度
//		DeptResult dept = apiGet.getDept();
//		//获取买方深度
//		List<Dept> asks = dept.getAsks();
//		//卖1详情
//		Dept ask1 = asks.get(0);
//		System.out.println("卖1,价:"+ask1.getPrice()+",量:"+ask1.getAmount());
	}
	
//	@Test
	public void 交易api() {
		System.out.println("行情api针对的交易对:"+apiPost.getSymbol());
		//挂单
		double price = apiGet.getDept().getBids().get(2).getPrice();//获取买4价格挂单
		TradeResult buyResult = apiPost.buy(price, 1);
		long id = buyResult.getId();//返回挂单id
		System.out.println("下单返回结果:"+buyResult+",挂单Id:"+id);
		//取消订单
		TradeResult cancelResult = apiPost.cancelOrder(id);
		System.out.println("取消订单结果:"+cancelResult);
		//查询订单id详情
		Order order = apiPost.getOrder(id);
		System.out.println("订单id:"+order.getId()+",详情:"+order);
		//查询未成交订单
		List<Order> orders = apiPost.getUnfinishedOrdersIgnoreTradeType(1);
		orders.forEach(o->{
			System.out.println("未成交订单:"+o);
		});
		
		//遍历用户信息
		Account account = apiPost.getAccount();
		account.getResult().getCoins().forEach(coin->{
			System.out.println("可用:"+coin.getAvailable()+",冻结:"+coin.getFreez());
		});
	}

	@Before
	public void init() throws IOException{
		String symbol = "zb_qc";
		//构造行情api对象
		apiGet = new RestApiGet(symbol);
		
		Props p = new Props();
		p.load(new File("c:/config/zb.txt"));
		String apiKey = p.getValue("user.zb.apikey");//修改为自己的公钥
		String secretKey= p.getValue("user.zb.secretKey");//修改为自己的私钥
		//构造交易接口对象
		apiPost = new RestApiPost(symbol, new UserApi(ExchangeEnum.zb, "测试", apiKey, secretKey));
	}
}
